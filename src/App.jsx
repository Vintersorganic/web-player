import React from "react";
import Player from "./components/Player";
import './styles.css';

const App = () => {
  return (
    <div className="container">
      <h1>Webpack working</h1>
      <Player />
    </div>
  );
};

export default App;
