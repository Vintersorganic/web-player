import React from 'react'
import ReactPlayer from 'react-player'

const Player = ({ width, url, controls }) => {
  return (
    <ReactPlayer width={width} url={url} controls={controls} light/>
  )
}

export default Player